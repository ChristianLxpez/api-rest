'use strict'

const port = process.env.PORT || 3000;

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const { reset } = require('nodemon');

const app = express();

var db = mongojs("SD-40");
var id = mongojs.ObjectID;

// Declaramos nuestros middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.param("colecciones", (req, res, next, coleccion) => {
    console.log('param /api/:colecciones');
    console.log(coleccion);

    req.collection = db.collection(coleccion);
    return next();
});

// Creamos un Middleware de Autorización tipo Bear
function auth(req, res, next) {
    if (!req.headers.authorization) {
        res.status(401).json({
            result: 'KO',
            mensaje: "No se ha enviado el token tipo Bearer en la cabecera authorization."
        });
        return next(new Error("Falta el token"));
    }

    if (req.headers.authorization.split(" ")[1] == "MITOKEN123456789") {
        return next();
    }
    
    res.status(401).json({
        result: 'KO',
        mensaje: "Acceso no autorizado a este servicio."
    });
    return next(new Error("Acceso no autorizado."));
}

// Declaramos nuestras rutas y nuestros controladores
app.get('/api', (req, res, next) => {
    console.log('GET /api');
    
    db.getCollectionNames((err, colecciones) => {
        if (err) return next(err);      // Propagamos el error

        console.log(colecciones);
        res.json({
            result: 'OK',
            colecciones: colecciones
        });
    });
});

app.get('/api/:colecciones', (req, res, next) => {
    
    req.collection.find((err, elementos) => {
        if (err) return next(err);      // Propagamos el error

        console.log(elementos);
        res.json({
            result: 'OK',
            coleccion: req.params.colecciones,
            elementos: elementos
        });
    });
});

app.get('/api/:colecciones/:id', (req, res, next) => {
    
    req.collection.findOne({_id: id(req.params.id)}, (err, elemento) => {
        if (err) return next(err);      // Propagamos el error

        console.log(elemento);
        res.json({
            result: 'OK',
            coleccion: req.params.colecciones,
            elemento: elemento
        });
    });
});

// creamos un elemento dentro de una colección
app.post('/api/:colecciones', auth, (req, res, next) => {
    const nuevoElemento = req.body;
    const queColeccion = req.params.colecciones;

    req.collection.save(nuevoElemento, (err, elementoGuardado) => {
        if (err) return next(err);

        console.log(elementoGuardado);
        res.status(201).json({
            result: 'OK',
            coleccion: queColeccion,
            elemento: elementoGuardado
        });
    });
});

app.put('/api/:colecciones/:id', auth, (req, res, next) => {
    const elementoID = req.params.id;
    const nuevosDatos = req.body;

    req.collection.update({_id: id(elementoID)}, 
    {$set: nuevosDatos},
    {safe: true, multi: false},
    (err, resultado) => {
        if (err) return next(err);

        console.log(resultado);
        res.json({
            result: 'OK',
            coleccion: req.params.coolecciones,
            resultado: resultado
        });
    });
});

app.delete('/api/:colecciones/:id', auth, (req, res, next) => {
    const elementoID = req.params.id;
    const queColeccion = req.params.colecciones;

    req.collection.remove({_id: id(elementoID)},
    (err, resultado) => {
        if (err) return next(err);

        console.log(resultado);
        res.json({
            result: 'OK',
            coleccion: queColeccion,
            elementoId: elementoID,
            resultado: resultado
        });
    });
});


app.listen(port, () => {
    console.log(`API REST CRUD con BD ejecutándose en http://localhost:${port}/api/{tabla}/{id}`);
});
